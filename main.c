#include	"config.h"
#include	"stdio.h"
#include	"string.h"
#include	"USART.h"
#include	"delay.h"
#include	"ADC.h"

/*************	本地常量声明	**************/
char xdata adc[10];
sbit open=P2^6;
sbit close=P2^7;
sbit ols=P3^2;
sbit cls=P3^3;
sbit runled=P5^5;
sbit openled=P2^1;
sbit closeled=P2^2;
sbit olsled=P2^3;
sbit clsled=P2^4;
sbit errled=P2^5;
/*************	本地变量声明	**************/
u8 time;
float xdata ch1,a,at,ch2,a1,in,ch0,a2,Dead_zone,ch3,ch4,a3,a4;
float xdata Zero_position,Full_position,Zero_signal,Full_signal;
float xdata position,setup;
/*************	本地函数声明	**************/



/*************  外部函数和变量声明 *****************/
//extern void read_dh(void);
void	UART_config(void)
{
	COMx_InitDefine		COMx_InitStructure;					//结构定义
	COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;		//模式,       UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
	COMx_InitStructure.UART_BRT_Use   = BRT_Timer2;			//使用波特率,   BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
	COMx_InitStructure.UART_BaudRate  = 115200ul;			//波特率, 一般 110 ~ 115200 使用timer2时，波特率需要与com2相同
	COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
	COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
	COMx_InitStructure.UART_Interrupt = ENABLE;				//中断允许,   ENABLE或DISABLE
	COMx_InitStructure.UART_Polity    = PolityLow;			//中断优先级, PolityLow,PolityHigh
	COMx_InitStructure.UART_P_SW      = UART1_SW_P30_P31;	//切换端口,   UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17(必须使用内部时钟)
	COMx_InitStructure.UART_RXD_TXD_Short = DISABLE;		//内部短路RXD与TXD, 做中继, ENABLE,DISABLE
	USART_Configuration(USART1, &COMx_InitStructure);		//初始化串口1 USART1,USART2

	COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;		//模式,       UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
	COMx_InitStructure.UART_BaudRate  = 115200ul;			//波特率,     110 ~ 115200
	COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
	COMx_InitStructure.UART_Interrupt = ENABLE;				//中断允许,   ENABLE或DISABLE
	COMx_InitStructure.UART_Polity    = PolityLow;			//中断优先级, PolityLow,PolityHigh
	COMx_InitStructure.UART_P_SW      = UART2_SW_P46_P47;	//切换端口,   UART2_SW_P10_P11,UART2_SW_P46_P47
	//USART_Configuration(USART2, &COMx_InitStructure);		//初始化串口2 USART1,USART2

	//PrintString1("System UART1 OK!\r\n");	//SUART1发送一个字符串，验证串口正常
	//PrintString2("System UART2 OK!\r\n");	//SUART2发送一个字符串
}


//void dog (void)
//{
//g=500;
//WDT_reset(6);//4.55S@11.0592M	
////WDT_reset(4);//1.14S@11.0592M	
//}
void timer0_int (void) interrupt TIMER0_VECTOR //timer0用来调度定时任务ms。
//未完成：执行任务开始后，暂停计时
{
	 	Timer0_Stop();
		ch1=GetADCResult(1); //位置反馈
		a=a*0.9+ch1*0.1;
		at=at*0.9+a*0.1;
		ch0=GetADCResult(0); //信号输入
		a1=a1*0.9+ch0*0.1;
		in=in*0.9+a1*0.1;
		ch2=GetADCResult(4)/100;  //死区
		a2=a2*0.9+ch2*0.1;
		Dead_zone=Dead_zone*0.9+a2*0.1;
		ch3=GetADCResult(5)/4;		 //零点
		a3=a3*0.9+ch3*0.1;
		Zero_signal=Zero_signal*0.9+a3*0.1;
		ch4=1023-GetADCResult(6)/4;	  //满点
		a4=a4*0.9+ch4*0.1;
		Full_signal=Full_signal*0.9+a4*0.1;
		if (time>0)time--;
		Timer0_Run();

}
/**********************************************/
void main(void)
{
	    open=1;
		close=1;
		openled=1;
		closeled=1;
		olsled=1;
		clsled=1;
		errled=1;
		runled=1;
		//delay_s(1);
		Timer0_16bitAutoReload();//16位模式
		Timer0_AsTimer();//定时器模式
		Timer0_1T();
		TL0 = 0x00;		//设置定时初值6ms
		TH0 = 0x00;		//设置定时初值
		Timer0_InterruptEnable();//使能定时器0
		//Timer0_Run();	//定时器0开始计时
		Timer0_Stop();
		UART_config();//串口配置
		//EX0=1;			//允许外部中断0	  红外用
		//IT0=1;			//外部中断0为下降沿触发	  红外用
		EA = 1;//使能总中断
		
		P1M1 = 0x00;   //将P1口设置为开漏输出ADC可以用开漏输出或高阻输入（12系列需要设置，15系列不需要设置。）	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	P1M0 = 0X00;
		P2M1 = 0x00;   //将P1口设置为开漏输出ADC可以用开漏输出或高阻输入（12系列需要设置，15系列不需要设置。）	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	P2M0 = 0X00;
		P3M1 = 0x00;   //将P1口设置为开漏输出ADC可以用开漏输出或高阻输入（12系列需要设置，15系列不需要设置。）	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	P3M0 = 0X00;
		P5M1 = 0x00;   //将P1口设置为开漏输出ADC可以用开漏输出或高阻输入（12系列需要设置，15系列不需要设置。）	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	P5M0 = 0X00;
		//P2M1 = 0x00;   //将P2口设置为推挽输出	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	//P2M0 = 0XFF;
		//P3M1 = 0x1C;   //将P3 234 口设置为输入	   M1=0 M0=1 推挽   10 高阻输入 00 双向 11开漏输出
	  	//P3M0 = 0XE0;	//P3 567 为PWM输出口，推挽
		InitADC();		//初始化ADC
		delay_ms(20);
		a=at=ch1=GetADCResult(1);
		delay_us(1);
		a1=in=ch0=GetADCResult(0);
		delay_us(1);
		a2=Dead_zone=ch2=GetADCResult(4)/100;
		Zero_signal=a3=ch3=GetADCResult(5)/4;
		Full_signal=a4=ch4=1023-GetADCResult(6)/4;
	Timer0_Run();	//完成初始化定时器0开始计时
	//读取配置
    	sprintf(adc,"%d",GetADCResult(0));
		PrintString1(adc);
		PrintString1(" 0\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(1));
		PrintString1(adc);
		PrintString1(" 1\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(2));
		PrintString1(adc);
		PrintString1(" 2\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(3));
		PrintString1(adc);
		PrintString1(" 3\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(4));
		PrintString1(adc);
		PrintString1(" 4\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(5));
		PrintString1(adc);
		PrintString1(" 5\r\n");
		delay_ms(20);
		sprintf(adc,"%d",GetADCResult(6));
		PrintString1(adc);
		PrintString1(" 6\r\n");
	Zero_position=200;
	Full_position=945;
	PrintString1("位置，输入，死区，相对位置，设置位置,零位，满位\r\n");
	while (1)
	{
		if (time<1){
		runled=~runled;
		sprintf(adc,"%.0f",at);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",in);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",Dead_zone);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.1f",position);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.1f",setup);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",Zero_signal);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",Full_signal);
		PrintString1(adc);
		PrintString1("\r\n");
		/***sprintf(adc,"%.0f",ch2);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",a1);
		PrintString1(adc);
		PrintString1(",");
		sprintf(adc,"%.0f",b1);
		PrintString1(adc);
		PrintString1("\r\n");***/
		time=5;
		}
		position=(at-Zero_position)*100/(Full_position-Zero_position);	//位置，%比
		setup=(in-Zero_signal)*100/(Full_signal-Zero_signal);		//设定值，%比
		if (ch0>100 &&ch0<1000){ //
		errled=1;
			if (setup-position>Dead_zone/10){	//open
				if(ols){
					open=0;
					openled=0;
					olsled=1;
				}else olsled=0;
				//PrintString1("open\r\n");
			}else{
				open=1;
				openled=1;
			}
			if (position-setup>Dead_zone/10){	//close
			    if(cls){
					close=0;
					closeled=0;
					clsled=1;
				}
				else clsled=0;
				//PrintString1("close\r\n");
			}else{
				close=1;
				closeled=1;
			}
		}else{
			open=1;
			close=1;
			openled=1;
			closeled=1;
			errled=0;
		}
	}
	
}




