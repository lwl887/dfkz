#define ADC_POWER   0x80            //ADC power control bit  10000000
#define ADC_FLAG    0x10            //ADC complete flag		 00010000
#define ADC_START   0x08            //ADC start control bit  00001000
#define ADC_SPEEDLL 0x00            //540 clocks				 00000000
#define ADC_SPEEDL  0x20            //360 clocks				 00100000
#define ADC_SPEEDH  0x40            //180 clocks				 01000000
#define ADC_SPEEDHH 0x60            //90 clocks					 01100000

void InitADC();
int GetADCResult(char ch);

void InitADC()
{
    P1ASF=0xFF;        //Set  P10-17 as A/D mode  11111111，通道作为AD转化用。
    ADC_RES = 0;                    //Clear previous result
	 ADC_RESL = 0;
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL;
    delay_ms(2);                       //ADC power-on and delay
}

int GetADCResult(char ch)
{
    ADC_CONTR = ADC_POWER | ADC_SPEEDLL | ch | ADC_START;
    _nop_();                        //Must wait before inquiry
    _nop_();
    _nop_();
    _nop_();
    while (!(ADC_CONTR & ADC_FLAG));//Wait complete flag
    ADC_CONTR &= ~ADC_FLAG;         //Close ADC
	 return (ADC_RES*4+ADC_RESL);		//Return 10 bit ADC result
	 //return ADC_R;                 //Return 8 bit ADC result
}